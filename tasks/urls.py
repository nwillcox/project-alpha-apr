from django.urls import path
from tasks.views import create_task, see_assigned_tasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", see_assigned_tasks, name="show_my_tasks"),
]
