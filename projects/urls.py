from django.urls import path
from projects.views import (
    list_all_projects,
    single_project_detail,
    create_project,
)

urlpatterns = [
    path("", list_all_projects, name="list_projects"),
    path("<int:id>/", single_project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
